import com.datageneration.BuildingsDataGeneratorForProtoObjects;
import com.datageneration.EmployeesDataGeneratorForProtoObjects;
import com.protoassignments.first.ProtoDataMain;
import com.protoassignments.fourth.AttendanceDriver;
import com.protoassignments.second.BulkLoadBuildingDriver;
import com.protoassignments.second.BulkLoadEmployeeDriver;
import com.protoassignments.third.EmployeeCafeteriaDriver;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.util.Bytes;

public class Main {

    public static void main(String args[]){

        String localPath = "/Users/devharsha/IdeaProjects/ThirdAssignment/output/*.csv";
        String hdfsPath = "/user/DevHarsha/ProtoObjects";

        Path cafeteriahdfsPath = new Path("/user/DevHarsha/PopulateCafeteria");
        Path attendancehdfsPath = new Path("/user/DevHarsha/EmployeeAttendance");
        Path bulkLoadPath = new Path("/user/DevHarsha/bulkload");

        Path building_csv = new Path("/user/DevHarsha/ProtoObjects/Building.csv");
        Path employee_csv = new Path("/user/DevHarsha/ProtoObjects/Employee.csv");


        int TOTAL_EMPLOYEES = 100;
        int TOTAL_BUILDINGS = 10;
        String total_floors = "5";
        String companies_in_building = "10";
        String BUILDINGS_TABLE = "buildings";
        String EMPLOYEES_TABLE ="employees";

        byte[] CF_NAME = Bytes.toBytes("information");

        /*Generate Employees Data and write it to CSV Files*/
        EmployeesDataGeneratorForProtoObjects protoObjects = new EmployeesDataGeneratorForProtoObjects(TOTAL_BUILDINGS);
        protoObjects.generate(TOTAL_EMPLOYEES,System.getProperty("user.dir")+"/output");
        protoObjects.copyFilesFromLocal(localPath,hdfsPath);

        /*Generate Buildings Data and write it to CSV Files*/
        BuildingsDataGeneratorForProtoObjects buildingObjects = new BuildingsDataGeneratorForProtoObjects(total_floors,companies_in_building);
        buildingObjects.generate(TOTAL_BUILDINGS,System.getProperty("user.dir")+"/output");
        buildingObjects.copyFilesFromLocal(localPath,hdfsPath);


        /*First path of assignment : Load csv data to proto objects and print using the protoObjects*/
        ProtoDataMain protoDataMain = new ProtoDataMain();
        protoDataMain.printProtoObjectsData();

        /*AlternateApproach alternateApproach = new AlternateApproach(TOTAL_EMPLOYEES,TOTAL_BUILDINGS);

        try {
            alternateApproach.run(args);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        /*Second part of assignment : Bulk Loading Buildings data & Employees data */
        BulkLoadBuildingDriver buildingDriver = new BulkLoadBuildingDriver(building_csv,bulkLoadPath,BUILDINGS_TABLE,CF_NAME);
        BulkLoadEmployeeDriver employeeDriver = new BulkLoadEmployeeDriver(employee_csv,bulkLoadPath,EMPLOYEES_TABLE,CF_NAME);

        /*Third part of assignment : Populating employees table with cafeteria code*/
        EmployeeCafeteriaDriver employeeCafeteriaPopulateDriver = new EmployeeCafeteriaDriver(cafeteriahdfsPath,EMPLOYEES_TABLE,BUILDINGS_TABLE);
        /*Fourth part of assignment :  Getting employee with lowest attendance*/
        AttendanceDriver attendanceDriver = new AttendanceDriver(attendancehdfsPath,EMPLOYEES_TABLE);


        try {

            buildingDriver.run(args); /*BulkLoading building data to HBase*/
            employeeDriver.run(args); /*BulkLoading Employee data to HBase*/
            employeeCafeteriaPopulateDriver.run(args); /*MapReduce job to populate employeeID with cafeteria code*/
            attendanceDriver.run(args); /*MapReduce job to get employee with Lowest attendance*/

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
