package com.datageneration;

import com.github.javafaker.Faker;
import com.opencsv.CSVWriter;
import util.AbstractDataGenerator;

import java.io.File;
import java.io.IOException;

public class BuildingsDataGeneratorForProtoObjects extends AbstractDataGenerator {

    String total_floors;
    String companies_in_building;

    public BuildingsDataGeneratorForProtoObjects(String total_floors,String companies_in_building){
        this.total_floors = total_floors;
        this.companies_in_building = companies_in_building;
    }

    @Override
    public void generate(int noOfEntries,String outputPath) {

        Faker fake;
        String[] building_header = {"building_code", "total_floors", "companies_in_the_building", "cafteria_code"};

        try {

            String buildings = "Building.csv";

            File dir = new File (outputPath);

            CSVWriter buildingWriter = getWriter(dir,buildings);

            buildingWriter.writeNext(building_header);

            //building_code, total_floors, companies_in_the_building, cafteria_code
            for(int i=0;i< noOfEntries;i++){
                fake  = new Faker();

                String[] building_data = {String.valueOf(i), total_floors, companies_in_building, String.valueOf(fake.hashCode())};
                buildingWriter.writeNext(building_data);

            }

            buildingWriter.close();

        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }

}
