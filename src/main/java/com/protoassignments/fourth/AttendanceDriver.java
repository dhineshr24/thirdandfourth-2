package com.protoassignments.fourth;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


//Need to create HDFS File /user/DevHarsha/EmployeeAttendance

public class AttendanceDriver extends Configured implements Tool {

    private Path OUTPUT;
    private byte[] TABLE_NAME;

    public AttendanceDriver(Path hdfsPath,String tableName){
        this.OUTPUT = hdfsPath;
        this.TABLE_NAME = Bytes.toBytes(tableName);
    }
    public static void main(String args[]) throws Exception {



    }
    @Override
    public int run(String[] args) throws Exception {

        List<Scan> scans = getScans();
        HBaseConfiguration conf = new HBaseConfiguration();
        Job job = getJob(conf);

        TableMapReduceUtil.initTableMapperJob(scans, AttendanceMapper.class, IntWritable.class,
                ImmutableBytesWritable.class, job);
        job.setReducerClass(AttendanceReducer.class);

        boolean success = job.waitForCompletion(true);

        return success?0:1;
    }

    private List<Scan> getScans() {
        List<Scan> scans = new ArrayList<>();
        Scan scan = new Scan();
        scan.setAttribute("scan.attributes.table.name", TABLE_NAME);
        scans.add(scan);
        return scans;
    }

    private Job getJob(HBaseConfiguration conf)  {

        Job job = null;
        try {

            job = Job.getInstance(conf, "Lowest Attendance");
            job.setJarByClass(AttendanceDriver.class);
            FileOutputFormat.setOutputPath(job, OUTPUT);
            job.setOutputKeyClass(IntWritable.class);
            job.setOutputValueClass(IntWritable.class);
            job.setJarByClass(AttendanceDriver.class);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return job;

    }
}
