package com.protoassignments.first;

import com.protoobjects.Building;
import com.protoobjects.Employee;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/*
Employee.proto
    Example fields can be : name, employee_id, building_code, floor_number (this should be enum), salary, department
Building.proto
    Example fields can be : building_code, total_floors, companies_in_the_building, cafteria_code

*/


public class AlternateApproach extends Configured implements Tool {

    private int TOTAL_EMPLOYEES;
    private int TOTAL_BUILDINGS;

    public AlternateApproach(int employees, int buildings){
        this.TOTAL_BUILDINGS = buildings;
        this.TOTAL_EMPLOYEES = employees;
    }

    @Override
    public int run(String[] args) throws IOException {

        Building buildings[] = new Building[TOTAL_BUILDINGS];
        Employee employees[] = new Employee[TOTAL_EMPLOYEES];


        //Function to load the data into Proto objects
        loadDataIntoProtoObjects(employees,buildings);

        for(int i=0;i<TOTAL_BUILDINGS;i++){
            System.out.println(buildings[i].getBuildingCode()+" "
                    +buildings[i].getTotalFloors()+" "+buildings[i].getCompaniesInTheBuilding()+" "
                    +buildings[i].getCafeteriaCode());
        }

        for(int i=0;i<TOTAL_EMPLOYEES;i++){
            System.out.println(employees[i].getName()+" "
                    +employees[i].getEmployeeId()+" "
                    +employees[i].getBuildingCode()+" "+employees[i].getFloor()+" "
                    +employees[i].getSalary()+" "+employees[i].getDepartment());
        }

        return 0;
    }


    private void loadDataIntoProtoObjects(Employee employee[],Building building[]) throws IOException {

        int employees = 0;
        int buildings = 0;

        String employee_file = System.getProperty("user.dir")+"/output/Employee.csv";
        File file = new File(employee_file);

        String building_file = System.getProperty("user.dir")+"/output/Building.csv";
        File bfile = new File(building_file);

        writeToObjects(employee, employees, file);
        writeToObjects(building, buildings, bfile);

    }


    private void writeToObjects(Building[] building, int buildings, File bfile) throws IOException {
        BufferedReader csvReader = new BufferedReader(new FileReader(bfile));
        csvReader.readLine();
        String lineRead = csvReader.readLine();

        while (lineRead!=null) {

            String building_info[] = lineRead.split(",");

            building[buildings++] = Building.newBuilder()
                    .setBuildingCode(Integer.parseInt(building_info[0].substring(1, building_info[0].length() - 1)))
                    .setTotalFloors(Integer.parseInt(building_info[1].substring(1, building_info[1].length() - 1)))
                    .setCompaniesInTheBuilding(Integer.parseInt(building_info[2].substring(1, building_info[2].length() - 1)))
                    .setCafeteriaCode(Integer.parseInt(building_info[3].substring(1, building_info[3].length() - 1)))
                    .build();

            lineRead = csvReader.readLine();
        }

        csvReader.close();
    }

    private void writeToObjects(Employee[] employee, int employees, File file) throws IOException {

        BufferedReader csvReader = new BufferedReader(new FileReader(file));
        csvReader.readLine();
        String lineRead = csvReader.readLine();

        while(lineRead!=null) {
            String[] data = lineRead.split(",");

            employee[employees++] = Employee.newBuilder()
                    .setName(data[0].substring(1, data[0].length() - 1))
                    .setEmployeeId(Integer.parseInt(data[1].substring(1, data[1].length() - 1)))
                    .setBuildingCode(Integer.parseInt(data[2].substring(1, data[2].length() - 1)))
                    .setFloor(Employee.FLOOR_NUMBER.valueOf(data[3].substring(1, data[3].length() - 1)))
                    .setSalary(Integer.parseInt(data[4].substring(1, data[4].length() - 1)))
                    .setDepartment(data[5])
                    .build();

            lineRead = csvReader.readLine();
        }
        csvReader.close();

    }


}
